package com.company;

public class Player {
    private String name;
    private int score =0;

    public Player(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score = this.score + score;
    }

    public boolean greaterThen (Player p){
        return (this.getScore() > p.getScore());
    }

    public String getName() {
        return name;
    }
}
