package com.company;

import java.util.Comparator;

class ScoreComperator implements Comparator<Player> {
    @Override
    public int compare(Player a, Player b) {
        return a.getScore() < b.getScore() ? -1 : a.getScore() == b.getScore() ? 0 : 1;
    }
}