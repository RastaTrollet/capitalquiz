package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        ArrayList<Player> playerList = new ArrayList<>();

        System.out.println("welcome to the internet, how many capitals do you know?");
        System.out.print("Push enter to start! ");
        do{
                sc.nextLine();

            System.out.print("Type your name: ");

            Player player = new Player(sc.nextLine());

            player.addScore(askQuestion(sc, "Vad heter Japans huvudstad?", "Tokyo"));
            player.addScore(askQuestion(sc, "Vad heter Sveriges huvudstad?", "Stockholm"));
            player.addScore(askQuestion(sc, "Vad heter Tysklands huvudstad?", "Berlin"));
            player.addScore(askQuestion(sc, "Vad heter Australiens huvudstad?", "Canberra"));
            player.addScore(askQuestion(sc, "Vad heter Kinas huvudstad?", "Peking"));

            playerList.add(player);

            Collections.sort(playerList, new ScoreComperator());
            Collections.reverse(playerList);

            for (Player p :playerList){
                System.out.println(p.getName() + " : " + p.getScore());
            }

            System.out.print("Are there more players? (y/n) ");

        }while ((sc.next().toLowerCase()).equals("y"));



        /*
        Player a = new Player("Abe");
        Player b = new Player ("Bob");
        Player c = new Player ("cat");
        a.addScore(4);
        b.addScore(2);
        c.addScore(6);

        ArrayList<Player> pl = new ArrayList<>();
        pl.add(a); pl.add(b); pl.add(c);



        for (Player p : pl){
            System.out.println(p.getName() + " : " + p.getScore());
        }
        */
    }

    public static int askQuestion(Scanner sc, String question, String rightAnser){
        int points = 3;
        System.out.println(question);
        do {
            if (sc.next().toLowerCase().equals(rightAnser.toLowerCase())){
                sc.nextLine();
                System.out.println("rätt");
                System.out.println();
                return points;
            }
            else {
                System.out.print("Fel!");
                sc.nextLine();
                System.out.println();
                points--;
                if (points > 0)
                    System.out.println("Försök igen");
                else
                    System.out.println("Vi går vidare, det här börjar bli pinsamt");
            }
        } while (points > 0);

        return points;
    }



}
